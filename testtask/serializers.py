from rest_framework import serializers
from .models import FieldType, Field, RiskType, Enumerations


def add_enum(item, field):
    if not item.get('id'):
        Enumerations.objects.create(name=item.get('name'), field=field)
    else:
        updated_item = Enumerations.objects.get(id=item.get('id'))
        updated_item.name = item.get('name')


class TypeOfField:
    Text = 1
    Number = 2
    Date = 3
    Enum = 4


class FieldTypeSerializer(serializers.Serializer):
    """Serializer for FieldType"""
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(required=False)

    class Meta:
        model = FieldType
        fields = 'id'


class EnumerationSerializer(serializers.Serializer):
    """Serializer for enum field"""
    id = serializers.IntegerField(required=False)
    name = serializers.CharField()

    class Meta:
        model = Enumerations
        fields = ('id', 'name')


class FieldSerializer(serializers.ModelSerializer):
    """Serializer for Field"""
    id = serializers.IntegerField(required=False)
    title = serializers.CharField(required=True)
    field_type = FieldTypeSerializer()
    enum_set = EnumerationSerializer(many=True)

    class Meta:
        model = Field
        fields = ('id', 'field_type', 'title', 'enum_set')


class RiskTypeSerializer(serializers.ModelSerializer):
    """Serializer for RiskType"""
    field_set = FieldSerializer(many=True)

    id = serializers.IntegerField(required=False)
    name = serializers.CharField()
    description = serializers.CharField(required=False, allow_blank=True)

    def create(self, validated_data):
        field_set = validated_data.pop('field_set')
        name = validated_data.pop('name')
        description = validated_data.pop('description')
        risk_type = RiskType.objects.create(name=name, description=description)

        for field in field_set:
            field_type = field.get('field_type')
            selected_type = FieldType.objects.get(id=field_type.get('id'))
            new_field = Field.objects\
                .create(field_type=selected_type, risk_type=risk_type, title=field.get('title'))

            if field_type.get('id') == 4 and len(field.get('enum_set')) > 0:
                for item in field.get('enum_set'):
                    add_enum(item, new_field)

        return risk_type

    def update(self, risk_type, validated_data):

        risk_type.name = validated_data.get('name', risk_type.name)
        risk_type.description = validated_data.get('description', risk_type.description)

        old_field_set = Field.objects.filter(risk_type=risk_type).all()
        new_field_set = [data.get('id') for data in validated_data.get('field_set')]

        """Delete fields"""
        for field in old_field_set:
            if field.id not in new_field_set:
                field.delete()

        for field in validated_data.get('field_set'):
            field_type = field.get('field_type')

            """Update fields"""
            if field.get('id'):
                updated_field = Field.objects.get(id=field.get('id'))
                updated_field.title = field.get('title', updated_field.title)
                updated_field.field_type.id = field_type.get('id', updated_field.field_type.id)

                updated_field.save()
            else:
                """Create fields"""
                selected_type = FieldType.objects.get(id=field_type.get('id'))
                new_field = Field.objects.create(title=field.get('title'), risk_type=risk_type,
                                                 field_type=selected_type)

                if field_type.get('id') == TypeOfField.Enum and len(field.get('enum_set')) > 0:
                    for item in field.get('enum_set'):
                        add_enum(item, new_field)

        risk_type.save()
        return risk_type

    class Meta:
        model = RiskType
        fields = ('id', 'name', 'description', 'field_set')
