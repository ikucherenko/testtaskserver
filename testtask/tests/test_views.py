import json
from rest_framework import status
from django.test import TestCase
from rest_framework.test import APIRequestFactory
from ..models import RiskType, FieldType, Field
from ..serializers import RiskTypeSerializer
from ..views import RiskTypeView


class GetAllRiskTypesTest(TestCase):
    """ Test module for GET all risk types API """

    def setUp(self):
        RiskType.objects.create(name='Test1', description='Test1')
        RiskType.objects.create(name='Test2', description='Test2')
        RiskType.objects.create(name='Test3', description='Test3')

    def test_get_all_risk_types(self):
        request = APIRequestFactory().get("")
        view = RiskTypeView.as_view({'get': 'list'})
        response = view(request)
        risk_types = RiskType.objects.all()
        serializer = RiskTypeSerializer(risk_types, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleRiskTypeTest(TestCase):
    """ Test module for GET single risk type API """
    def setUp(self):
        self.risk1 = RiskType.objects.create(name='Test1', description='Test1')
        self.risk2 = RiskType.objects.create(name='Test2', description='Test2')
        self.risk3 = RiskType.objects.create(name='Test3', description='Test3')

    def test_get_valid_single_risk_type(self):
        request = APIRequestFactory().get("", args=self.risk2.id)
        view = RiskTypeView.as_view({'get': 'retrieve'})
        response = view(request, pk=self.risk2.id)
        serializer = RiskTypeSerializer(self.risk2)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_risk_type(self):
        request = APIRequestFactory().get("", args=20)
        view = RiskTypeView.as_view({'get': 'retrieve'})
        response = view(request, pk=20)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewRiskTypeTest(TestCase):
    """ Test module for inserting a new risk type """
    def setUp(self):
        enum = FieldType.objects.create(name='enum', id=4)

        self.valid_payload = {
            'name': 'Test1',
            'description': 'Test1',
            'field_set': [
                {
                    "field_type": {
                        "id": enum.id,
                    },
                    "title": "ENUM",
                    "enum_set": [
                        {
                            "name": "test"
                        }
                    ]
                }
            ]
        }

        self.invalid_payload = {
            'name': '',
            'description': ''
        }

    def test_create_valid_risk_type(self):
        request = APIRequestFactory().post("", data=json.dumps(self.valid_payload),
                                           content_type='application/json')
        view = RiskTypeView.as_view({'post': 'create'})
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_risk_type(self):
        request = APIRequestFactory().post("", data=json.dumps(self.invalid_payload),
                                           content_type='application/json')
        view = RiskTypeView.as_view({'post': 'create'})
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateSingleRiskTypeTest(TestCase):
    """ Test module for updating an existing risk type record """

    def setUp(self):
        self.field_type1 = FieldType.objects.create(id=1, name='text')
        self.field_type2 = FieldType.objects.create(id=2, name='number')
        self.field_type4 = FieldType.objects.create(id=4, name='enum')
        self.risk1 = RiskType.objects.create(name='Test1', description='Test1')
        self.risk2 = RiskType.objects.create(name='Test2', description='Test2')
        self.risk3 = RiskType.objects.create(name='Test3', description='Test3')
        self.field3 = Field.objects.create(field_type=self.field_type2, title='TestField3',
                                           risk_type=self.risk2)

        self.valid_payload = {
            'name': 'Test1',
            'description': 'Test1',
            'field_set': [
                {
                    'title': 'test',
                    'field_type': {
                        'id': 1
                    },
                    'enum_set': []
                },
                {
                    'title': 'test1',
                    'field_type': {
                        'id': 2
                    },
                    'enum_set': [
                        {'name': '1'},
                        {'name': '2'}
                    ]
                },
            ]
        }

        self.invalid_payload = {
            'name': '',
            'description': ''
        }

    def test_valid_update_risk_type(self):
        request = APIRequestFactory().put("", data=json.dumps(self.valid_payload),
                                          content_type='application/json',
                                          kwargs={'pk': self.risk2.id})
        view = RiskTypeView.as_view({'put': 'update'})
        response = view(request, pk=self.risk2.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_risk_type(self):
        request = APIRequestFactory().put("", data=json.dumps(self.invalid_payload),
                                          content_type='application/json',
                                          kwargs={'pk': self.risk2.id})
        view = RiskTypeView.as_view({'put': 'update'})
        response = view(request, pk=self.risk2.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeleteSingleRiskTypeTest(TestCase):
    """ Test module for deleting an existing risk type record """

    def setUp(self):
        self.risk1 = RiskType.objects.create(name='Test1', description='Test1')
        self.risk2 = RiskType.objects.create(name='Test2', description='Test2')
        self.risk3 = RiskType.objects.create(name='Test3', description='Test3')

    def test_valid_delete_risk_type(self):
        request = APIRequestFactory().delete("", content_type='application/json',
                                             kwargs={'pk': self.risk2.id})
        view = RiskTypeView.as_view({'delete': 'destroy'})
        response = view(request, pk=self.risk2.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_risk_type(self):
        request = APIRequestFactory().delete("", content_type='application/json',
                                             kwargs={'pk': 20})
        view = RiskTypeView.as_view({'delete': 'destroy'})
        response = view(request, pk=20)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

