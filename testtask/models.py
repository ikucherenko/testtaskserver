from django.db import models


class RiskType(models.Model):
    """Model of risk type"""
    id = models.AutoField(db_column='Id', primary_key=True, editable=False, null=False)
    name = models.CharField(db_column='Name',  max_length=200)
    description = models.TextField(db_column='Description')

    class Meta:
        db_table = 'RiskTypes'


class FieldType(models.Model):
    """Model of field type"""
    id = models.AutoField(db_column='Id', primary_key=True, editable=False, null=False)
    name = models.CharField(db_column='Name', max_length=200)

    class Meta:
        db_table = 'FieldTypes'


class Enumerations(models.Model):
    """Model of enum field"""
    id = models.AutoField(db_column='Id', primary_key=True, editable=False, null=False)
    name = models.CharField(db_column='Name', max_length=200)
    field = models.ForeignKey('Field', db_column='Field', related_name='enum_set')

    class Meta:
        db_table = 'Enumerations'


class Field(models.Model):
    """Model of field"""
    id = models.AutoField(db_column='Id', primary_key=True, editable=False, null=False)
    risk_type = models.ForeignKey('RiskType', db_column='RiskType', related_name='field_set')
    field_type = models.ForeignKey('FieldType', db_column='FieldType')
    title = models.CharField(db_column='Title', max_length=100, null=True)

    class Meta:
        db_table = 'Fields'
