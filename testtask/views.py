from .models import RiskType
from rest_framework import viewsets, permissions
from .serializers import RiskTypeSerializer


class RiskTypeView(viewsets.ModelViewSet):
    """View for type of risk"""
    permission_classes = (permissions.AllowAny,)
    serializer_class = RiskTypeSerializer
    queryset = RiskType.objects.all()


