"""testtask URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from django.views import static

from testtask import settings
from . import views


app_name = 'testtask'

router = DefaultRouter(trailing_slash=False)
router.register(r'risk_type', views.RiskTypeView, 'risk-types')

basepatterns = [
    url(r'^admin/', admin.site.urls),
    url('', include(router.urls)),
]

# static files (images, css, javascript, etc.)

staticpatterns = [
    url(r'^media/(?P<path>.*)$', static.serve, {
        'document_root': settings.MEDIA_ROOT}),

    url(r'^static/(?P<path>.*)$', static.serve, {
        'document_root': settings.STATIC_ROOT})
]

urlpatterns = basepatterns + staticpatterns
