import sqlite3
conn = sqlite3.connect('db.sqlite3')

c = conn.cursor()

"""Add basic field types to DB"""
c.execute("INSERT INTO FieldTypes(Id, Name) SELECT 1, 'Text' WHERE NOT EXISTS (SELECT 1 FROM FieldTypes WHERE Id = 1)")
c.execute("INSERT INTO FieldTypes(Id, Name) SELECT 2, 'Number' WHERE NOT EXISTS (SELECT 1 FROM FieldTypes WHERE Id = 2)")
c.execute("INSERT INTO FieldTypes(Id, Name) SELECT 3, 'Date' WHERE NOT EXISTS (SELECT 1 FROM FieldTypes WHERE Id = 3)")
c.execute("INSERT INTO FieldTypes(Id, Name) SELECT 4, 'Enum' WHERE NOT EXISTS (SELECT 1 FROM FieldTypes WHERE Id = 4)")


conn.commit()

