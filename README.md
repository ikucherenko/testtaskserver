This is the backend part of the risk type application. It is created as RESTfull application with next API: 

	Path: 
	"/risk_types/((?P<risk_type_id>\d+)/)?"

	risk_type_id is optional for GET and required for PUT and DELETE
	
	GET:
		1. risk_type_id Provided:
			A. No additional query parameters required.
			B. Returns "RiskType" object if object with such id is exists.
			C. 404 will be raised otherwise.
			
		2. risk_type_id Not Provided:
			A. No additional query parameters required.
			B. Returns list of all "RiskType" objects.
			C. Returns empty set if no one risk type is exist.
			
	POST:
		1. add new risk type
			A. Takes "RiskType" object in the request payload which will be saved in the db.
			B. Returns newly created "RiskType" object with identities dilled in.
			C. Returns 403 in case of any error.
	
	PUT:
		1. Takes an "RiskType" object in the reques payload which will be updated in the db.
		2. Returns updated "RiskType" object.
			
			
	DELETE: 
		1. Deletes "RiskType" object and all dependencies (Fields, Enums)

This part of application has been created by Django Rest Framework v-3.6.3 and SQLite is used as DB. 


